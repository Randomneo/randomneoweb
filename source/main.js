var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var messages = {
    "#msg1": "Hello. I'm Randomneo. I like game and web development. Fun of GNU/Linux. Also spend some free time plaing on piano.",
    "#msg2": "For game development I use C++, Python, Lua.",
    "#msg3": "For web development - Python, Django, PHP, CSS, HTML, JS.",
    "#msg4": "You can find more info about my work in projects tab."
};
var msg_list = Object.keys(messages);
var msg_num = 0;

var showText = function (target, message, index, interval) {   
  if (index < message.length) {
    $(target).append(message[index++]);
    setTimeout(function () { showText(target, message, index, interval); }, interval);
  }
  else {
      printMessages();
  }
}

var printMessages = function() {
    if (msg_num >= msg_list.length)
        return;
    msg = msg_list[msg_num++];
    showText(msg, messages[msg], 0, 50);
}

jQuery(document).ready(function () {
  
    if(!(isMobile.any())) 
        printMessages(); 
    else
        for (var key in messages) {
            $(key).text(messages[key]);
        }  
});