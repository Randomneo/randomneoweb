<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>Randomneo</title>
        <link rel="stylesheet" href="./style/main.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script type="text/javascript" src="./source/main.js"></script>
    </head>
    <body>
    <div class="menubar">
        <ul>
            <li><a class="active" href="#">Home</a></li>
            <li><a href="projects.html">Projects</a></li>
        </ul>
    </div>
    <div class="main">
        <div class="mainbg">
            <div class="maincontent">
                <div class="box">
                    <p id="msg1"></p>
                    <p id="msg2"></p> 
                    <p id="msg3"></p>
                    <p id="msg4"></p>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
